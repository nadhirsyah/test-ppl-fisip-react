import React from 'react';
import './App.css';

function HelloWorld() {
  return (
    <div className="center-me">
      Hello World!
    </div>
  );
}

export default HelloWorld;
